## Description

This project allows the user to build and run a containerized version of Differentially Private Set Intersection (DiPSI). The code performs either set intersection, or calculates set intersection cardinality based on the mode chosen. The instructions given below guide a user through the process of building and running the container for the project, and running a sample benchmark. The container can be explored and has the complete source code of the project.

Since the project is containerized, all dependencies are resolved in the Dockerfile, and as such, only requires installing docker for your OS version.


## Build and run image from github using docker

As a preliminary step, install and run docker from https://docs.docker.com/get-docker/ based on the OS you are running. 

#### Clone the repo

```git clone https://git.uwaterloo.ca/jpremkum/dipsi.git```

Change directory to dipsi and run the following commands. Please note that your docker daemon needs to be running (for example, by using docker dashboard on windows). 

##### Note: docker build and docker run commands may require the use of sudo on linux

#### (optional) Change mode to Cardinality

Uncomment the appropriate lines in the dockerfile, to change the mode from Intersection to cardinality

#### Build the image:
```docker build --tag dipsi:1.0 .```


#### Run the container:
```docker run --name mycontainer -it dipsi:1.0```

Where "mycontainer" is the name of the container. You can choose any other name for your container, for example "testcontainer1". 

This command will run the container in interactive mode, and opens a command line interface to the container in-line. The benchmark commands are used on this command line. 
to exit this interactive mode, use the command ```exit```.

### Running benchmarks:

To run the benchmarks, navigate to the bin folder using 

```cd DiPSI```

```cd bin```


Run the below benchmark (same benchmark for either mode):

```./PSImain.cpp_exe -n 128 -m 1000 -b 10 -L 300```

Parameters:

```
-n : client database size
-m: server database size
-b : bitlength
-L : modulus chain length
```


The benchmark takes a few minutes to run. The matches in your result should match the ones given below (based on the mode chosen)

for INTERSECTION mode:

 Matches: [
       
       Matches on level 0: 25 66 37 98 58 120 45 94 54 78 36 15 50 86 9 16 82 77 8 99 73 67 60 52 48 28 90 0 2 121 95 39 56 32 3 30 29 18 111 44 5 124 21 31 102 11 51 49 74 12 42 46 34 7 93 17 112 41 65 23 27 55 10 107 38 100 24 118 35 81 13 61 6 4 40 20
      
       Matches on level 1: 79 113 96 115 108 103 101 92 70 125 122
     
       Matches on level 2: 76 772 91 22 62 116 97 33 47 119 109 64 63 53 75 43 68 87 69 88 26 1 85 117 57 824 114 89 59 72 104 105 14 123 19 71 84 106 110 80 83
     
       Matches on level 3:
]

Done!

for CARDINALITY mode:

(Client) Matches cardinality: 128

Done!



