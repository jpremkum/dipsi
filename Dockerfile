FROM ubuntu:18.04
WORKDIR /usr/src/app


# preliminaries and git

RUN apt-get update && apt-get -y install curl
RUN apt-get install wget
RUN apt-get update && apt-get -y install make cmake gcc g++ m4 libgmp-dev
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && apt-get -y install git
RUN apt-get update && apt-get install -y xz-utils
RUN apt-get update && apt-get install -y python-pip
RUN pip install cmake --upgrade

#obtain repository

RUN git clone https://github.com/cryspuwaterloo/DiPSI.git

# Armadillo

RUN apt install -y libopenblas-dev liblapack-dev
RUN wget http://sourceforge.net/projects/arma/files/armadillo-9.880.1.tar.xz
RUN tar -xvf armadillo-9.880.1.tar.xz
RUN cd armadillo-9.880.1 \
    && ./configure \
    && make \
    && make install

# NTL 10.3.0
RUN wget https://www.shoup.net/ntl/ntl-10.3.0.tar.gz
RUN tar xf ntl-10.3.0.tar.gz 
RUN cd ntl-10.3.0/src/ && ./configure \
    && make && make install \
    && cd ../..

RUN rm -rf ntl-10.3.0 && rm ntl-10.3.0.tar.gz

# libhcs
RUN git clone https://github.com/tiehuis/libhcs \
    && cd libhcs \
    && cmake . \
    && make \
    && make install

# update ntl location

RUN cd DiPSI \
    && sed -i "s,set(NTL_HEADER /home/nlukas/anaconda3/envs/psi_helib/include),set(NTL_HEADER /usr/local/include/NTL)," CMakeLists.txt \
    && sed -i "s,set(NTL_LIB /home/nlukas/anaconda3/envs/psi_helib/lib),set(NTL_LIB /usr/local/lib/libntl.a)," CMakeLists.txt

# OpenSSL

RUN apt-get install libssl-dev

# uncomment the below lines to change mode from INTERSECTION to CARDINALITY

#RUN cd DiPSI \
#    && cd src \
#    && sed -i "s, Mode mode = INTERSECTION;, Mode mode = CARDINALITY;," params.h


# Compilation

RUN cd DiPSI && cmake .
RUN cd DiPSI && cd src && make
